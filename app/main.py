from flask import Flask
from flask import json
from flask import Response
from flask_api import status
import socket

app = Flask(__name__)
	
@app.errorhandler(status.HTTP_404_NOT_FOUND) 
  
def not_found(e):
	resp = Response('URI not mapped to a resource', status.HTTP_404_NOT_FOUND, mimetype='text/plain')
	return resp 
	
@app.route("/api/myip/")
def showIPAddr():
	hostname = socket.gethostname()
	ipAddr = socket.gethostbyname(hostname)
	#data = {'Your client\'s IP address is': ipAddr}
	#js = json.dumps(data)
	#resp = Response(js, status.HTTP_200_OK, mimetype='application/json')
	resp = Response('Your client\'s IP address is: ' + ipAddr, status.HTTP_200_OK, mimetype='text/plain')
	return resp
    
if __name__ == "__main__":
	app.run(host='0.0.0.0',port=5000)