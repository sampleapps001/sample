pipeline {
    agent any
	//The CI/CD pipeline logic can be differentiaed based on the Git branch name patterns and deployment environment
	//The need of parameters is ignored here as the pipeline is targeted at only one environment and don't see anything
	//in specific to be parameterized
	environment { 
        GIT_URL = 'https://sampleapps001@bitbucket.org/sampleapps001/sample.git'
		GIT_CRED = 'bitbucket'
		DOCKER_CRED = 'dockerhub'
		APP_BASE_URL_1 = 'http://localhost:5000/api/myip'
		APP_BASE_URL_2 = 'http://localhost:5000/'
    }
    stages {
        stage('build') {
            steps {
				git changelog: true, credentialsId: env.GIT_CRED, poll: false, url: env.GIT_URL
				sh "pip3 install -r requirements.txt"
			}
		}
		stage('test') {
            steps {
				sh '''
					chmod +x tests.sh
					sh tests.sh
				'''
			}
		}
		stage('packaging') {
            steps {
				script {
					docker.withRegistry('https://registry.hub.docker.com', env.DOCKER_CRED) {
						def dockerfile = 'Dockerfile'
						def customImage = docker.build("csrini/python-sampleapp:latest", "-f ${dockerfile} .")
						customImage.push()
					}
				}
			}
		}
		/*The below mentioned stages can also be included
			1.Sonar code coverage analysis
			2.Fortify Security Vulnerabilities check
			3.Integration tests can be invoked on the generated artifact
			4.Uploading the generated artifact to Artifactories like JFrog or Nexus -- this step is of meaningful
			  when another application has a dependency on this artifact. The generation of an artifact with its
			  corresponding version number can also be achieved by pulling the code from code repository (It is 
			  assumed that the team is following git best pratice of branching strategy and others), so if 
			  there is no other application that has a dependency the uploading of artifact can be avoided.
			5.Deployment of the artifact to the corresponding physical/virtual machines either using a tool like
			XL Deploy or Netflix Spinnaker if the application deployment needs Zero downtime deployments or other
			sophisticated approaches like Rolling updates, Blue-Green, A/B testing and Canary
			6.The post deployement notificatins can be sent to multiple channels like Slack or Flowdock, emails
			7.The code commit information can be updated in tools like Agile Central, Jira along with creating a
			ticket/incident in ITSM tools like ServiceNow
		*/	
    }
	post {
		success {
			emailext body: '', recipientProviders: [developers()], subject: 'Jenkins build status notification - SUCCESS', to: 'vaas.mcajntk@gmail.com'
		}
		failure {
			emailext body: '', recipientProviders: [developers()], subject: 'Jenkins build status notification - FAILED', to: 'vaas.mcajntk@gmail.com'
		}
   }
}