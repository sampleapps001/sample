This sample application consists of an url path /api/myip which prints the ip address of the 
local machine and for all other urls it just prints the http 404 status along with the 
message "URI not mapped to a resource"

Jenkinsfile
------------
This repo includes of the working Jenkinsfile to automate the CI/CD flow for the application.
The steps that are automated includes
1) Downloading of code from Bitbucket
2) Building the application using pip command
3) Testing of the application using cURL command - one positive and negative scenario each
4) Containerizing the application
4) Post build action of sending email in case of success and failed scenario's.

There are few stages that could be incorporated but ignored for the sake of simplicity which includes the following
1.Sonar code coverage analysis
2.Fortify Security Vulnerabilities check
3.Integration tests can be invoked on the generated artifact
4.Uploading the generated artifact to Artifactories like JFrog or Nexus -- this step is of meaningful
	when another application has a dependency on this artifact. The generation of an artifact with its
	corresponding version number can also be achieved by pulling the code from code repository (It is 
	assumed that the team is following git best pratice of branching strategy and others), so if 
	there is no other application that has a dependency the uploading of artifact can be avoided.
5.Deployment of the artifact to the corresponding physical/virtual machines either using a tool like
	XL Deploy or Netflix Spinnaker if the application deployment needs Zero downtime deployments or other
	sophisticated approaches like Rolling updates, Blue-Green, A/B testing and Canary
6.The post deployement notificatins can be sent to multiple channels like Slack or Flowdock, emails
7.The code commit information can be updated in tools like Agile Central, Jira along with creating a
ticket/incident in ITSM tools like ServiceNow


Dockerfile
----------
The Dockerfile in this repo packages the following installations and the corresponding configurations for them
1) Python
2) Flask application
3) Nginx
4) uWSGI
5) Supervisord

The entrypoint.sh file invokes of start.sh under scripts folder which in turn will invoke of the pre-start commands 
in prestart.sh file. The start.sh file will start the supervisord daemon which inturn will start uWSGI and Nginx daemons.
The supervisord daemon is configured monitor for the status of the uWSGI and Nginx daemons and will start if they fail.

The Nginx configuration is configured in the entrypoint.sh file as per the environment variable values which could be 
provided while running the container. Most of the settings related to Nginx and uWSGI could be optimized as per the application
needs like for eg: setting of the processes and threads count in uWSGI configuration as this is heavily dependent on the no. of 
the cores of the machine on which the application is running and few options are meant to be configured on whether the Nginx is
proxying a multi-site or single site application(s).
https://blog.codeship.com/getting-every-microsecond-out-of-uwsgi/

This Nginx here is configured to serve static content (CDN) and reverse proxy a flask application. To utilise of the load-balancing
capabilities of Nginx, we need to configure Nginx in a seperate docker image from application which could be scaled as per the 
application needs, for which we also need to use any container orchestration tools like kubernetes, docker swarm, openshit and nomad.

The steps to run this dockerized application is as mentioned below.

docker run -d --name app -p 80:80 csrini/python-sampleapp:latest

To populate the static content run the below mentioned content
docker run -d --name app -p 80:80 -v $(pwd)/app:/app csrini/python-sampleapp:latest

sample o/p of the curl commands is provided below

curl -Lis localhost:80/api/myip:
--------------------------------
HTTP/1.1 308 PERMANENT REDIRECT
Server: nginx/1.15.3
Date: Tue, 26 Nov 2019 12:43:16 GMT
Content-Type: text/html; charset=utf-8
Content-Length: 259
Connection: keep-alive
Location: http://localhost/api/myip/

HTTP/1.1 200 OK
Server: nginx/1.15.3
Date: Tue, 26 Nov 2019 12:43:16 GMT
Content-Type: text/plain; charset=utf-8
Content-Length: 39
Connection: keep-alive

Your client's IP address is: 172.17.0.2

curl -Lis localhost:80/api:
---------------------------
HTTP/1.1 404 NOT FOUND
Server: nginx/1.15.3
Date: Tue, 26 Nov 2019 12:43:30 GMT
Content-Type: text/plain; charset=utf-8
Content-Length: 28
Connection: keep-alive

URI not mapped to a resource

curl -Lis localhost:5000/api/myip:(Not proxied as the port 5000 is used here - notice the changes of ip address)
---------------------------------
127.0.0.1 - - [26/Nov/2019 14:43:49] "GET /api/myip HTTP/1.1" 308 -
HTTP/1.0 308 PERMANENT REDIRECT
Content-Type: text/html; charset=utf-8
Content-Length: 269
Location: http://localhost:5000/api/myip/
Server: Werkzeug/0.16.0 Python/3.7.5
Date: Tue, 26 Nov 2019 12:43:49 GMT

127.0.0.1 - - [26/Nov/2019 14:43:49] "GET /api/myip/ HTTP/1.0" 200 -
HTTP/1.0 200 OK
Content-Type: text/plain; charset=utf-8
Content-Length: 38
Server: Werkzeug/0.16.0 Python/3.7.5
Date: Tue, 26 Nov 2019 12:43:49 GMT

Your client's IP address is: 127.0.1.1
