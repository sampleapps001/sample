#!/bin/bash
running=$(lsof -i -P -n | grep 5000)
if [ $? -eq 0 ]
then
	port=$(fuser -n tcp -k 5000)
	echo "App already running is killed"
fi

python app/main.py &
					
#Test of success scenario
echo "\n\n Executing the test case for success scenario: ${APP_BASE_URL_1}\n\n"
curl -Lis ${APP_BASE_URL_1}
curl -Lis ${APP_BASE_URL_1} | grep -q "200"

if [ $? -ne 0 ]; then
	echo "The test case to check for valid URL has failed"
	exit 1
fi
					
#Test of failure scenario
echo "\n\n Executing the test case for failure scenario: ${APP_BASE_URL_2}\n\n"
curl -Lis ${APP_BASE_URL_2}
curl -Lis ${APP_BASE_URL_2} | grep -q "404"

if [ $? -ne 0 ]; then
	echo "The test case to check for invalid URL has failed"
	exit 1
fi
